export interface Airline {
    site: string;
    code: string;
    alliance: string | null;
    phone: number;
    name: string;
    usName: string;
    defaultName: string;
    logoURL: string
}