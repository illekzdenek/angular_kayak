import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainContentComponent } from './main-content/main-content.component';
import { AirlineDetailComponent } from './airline-detail/airline-detail.component';

const routes: Routes = [
  { path: 'airlines', component: MainContentComponent },
  { path: 'detail/:id', component: AirlineDetailComponent },
  { path: '', redirectTo: '/airlines', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }