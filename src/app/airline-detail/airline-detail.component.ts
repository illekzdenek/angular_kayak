import { Component, Input, OnInit } from '@angular/core';
import { Airline } from '../airline';
import { AirlineService } from '../airline.service';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-airline-detail',
  templateUrl: './airline-detail.component.html',
  styleUrls: ['./airline-detail.component.scss']
})
export class AirlineDetailComponent implements OnInit {
  airline: Airline | null = null;

  constructor(
    private route: ActivatedRoute,
    private airlineService: AirlineService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getAirline()
  }

  goBack(): void {
    this.location.back();
  }

  
  getAirline(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.airlineService.getAirline(id)
      .subscribe(airline => {
        this.airline = airline
      });
  }

}
