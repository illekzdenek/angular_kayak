import { Component, Input, OnInit } from '@angular/core';
import { Airline } from '../airline';

@Component({
  selector: 'app-airline',
  templateUrl: './airline.component.html',
  styleUrls: ['./airline.component.scss']
})
export class AirlineComponent implements OnInit {
  @Input() airline?: Airline;

  constructor() { }

  ngOnInit(): void {
  }

  
  isFavorite(code:string): boolean{
    return localStorage.getItem(code) === "true" ? true : false
  }

  toFavorites($event: Event, code: string, favorite:boolean): void {
    $event.preventDefault()
    $event.stopPropagation()
    localStorage.setItem(code, favorite.toString())
  }

}
