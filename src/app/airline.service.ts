import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Airline } from './airline';

import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AirlineService {
  private url = 'https://www.kayak.com/h/mobileapis/directory/airlines';
  private coreURL = "https://www.kayak.com"
  constructor(private http: HttpClient) { }

  getAirlines(): Observable<Airline[]> {
    return this.http.get<Airline[]>(this.url)
    .pipe(
      map((data: any) => {
        data.logoURL = this.coreURL+data.logoURL
        for (let d of data){
          d.logoURL = this.coreURL+d.logoURL
        }
        return data;
      }),
      catchError(this.handleError<Airline[]>('getAirlines', []))
    );
  }

  getAirline(id: string | null): Observable<Airline> {

    return this.http.get<Airline>(this.url).pipe(
      map((data: any) => {
        if (id !== null){
          for (let d of data){
            if (d.code === id){
              d.logoURL = this.coreURL+d.logoURL
              return d
            }
          }
        }
        
        return null;
      }),
      catchError(this.handleError<Airline>(`getAirline id=${id}`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
