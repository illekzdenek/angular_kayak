import { Component, OnInit } from '@angular/core';
import { Airline } from '../airline';
import { AirlineService } from '../airline.service';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {
  airlines: Airline[] = [];
  page: number = 0;
  maxPages: number = 1;
  airlinesPerPage: number = 15
  favoriteView: boolean = false
  favoriteAirlines: Airline[] = []

  constructor(private airlineService: AirlineService) { }

  ngOnInit(): void {
    if (window.innerWidth <=768){
      this.airlinesPerPage = 4 
    }
    this.getAirlines()
    this.getFavoritesAirlines()
    
  }

  getFavoritesAirlines(): void{
    let favoriteAirlines = []
    for (let airline of this.airlines){
      if(localStorage.getItem(airline.code) === "true"){
        favoriteAirlines.push(airline)
      }
    }

    this.favoriteAirlines = favoriteAirlines;
  }

  getAirlines(): void {
    this.airlineService.getAirlines()
    .subscribe(airlines => {
      this.airlines = airlines
      const maxPages = airlines.length/this.airlinesPerPage
      this.maxPages = Number.isInteger(maxPages) ? Math.floor(maxPages)-1 : Math.floor(maxPages)
    });
  }

  getAirlinesByPage(): Airline[] {
    if (this.favoriteView){
      const maxPages = this.favoriteAirlines.length/this.airlinesPerPage
      this.maxPages = Number.isInteger(maxPages) ? Math.floor(maxPages)-1 : Math.floor(maxPages)
      
      return this.favoriteAirlines.slice(this.page*this.airlinesPerPage, (1+this.page)*this.airlinesPerPage);
    }
    const maxPages = this.airlines.length/this.airlinesPerPage
    this.maxPages = Number.isInteger(maxPages) ? Math.floor(maxPages)-1 : Math.floor(maxPages)
    return this.airlines.slice(this.page*this.airlinesPerPage, (1+this.page)*this.airlinesPerPage);
  }

  getNearPages(i: number): number[] {
    let arrayOfNearbyPages = []

    if (i-1 >= 0){
      if (i-2 >= 0){
        arrayOfNearbyPages.push(i-2)
      }
      arrayOfNearbyPages.push(i-1)
    }
    
    arrayOfNearbyPages.push(i)

    if (i+1 <= this.maxPages){
      arrayOfNearbyPages.push(i+1)
      if (i+2 <= this.maxPages){
        arrayOfNearbyPages.push(i+2)
      }
    }
    return arrayOfNearbyPages;
  }

  switchViewType():void{
    this.getFavoritesAirlines()
    this.favoriteView = !this.favoriteView
    this.changePage(0)
  }

  changePage(page:number) : void{
    this.page = page
    
    this.getNearPages(page)
  }

  pageMinusTen():void{
    this.page = this.page-10
  }
  pagePlusTen():void{
    this.page = this.page+10
  }

}
